"""
See fail sisaldab teise nädala ülesandeid.
Ülesannete eestikeelsed tekstid leiab Koodiklubi kodulehelt Teise nädala alt.
"""

# ===Exercise 1. === #

# Make a variable with the name "name" and ask for user input for its value
# Make a variable with the name "age" and ask for user input for its value
# Make a variable with at least three words in it's name and make it so it's value is an int (choose it yourself)
# Make a variable - choose the name yourself, but its value has to be True
name = input("Sisesta nimi: ")
age = input("Sisesta vanus: ")
need_kolm_sona = 15
kas_on_tosi = True





# === Exercise 2. === #

# Make 2 variables which are both of type int - choose the value yourself
# Add the variables and save the result in a new variable
# Subtract the variables and save the result in a new variable
# Multiply the variables and save the result in a new variable
# Divide the variables and save the result in a new variable
var1 = 15
var2 = 20
var3 = var1 + var2
var4 = var1 - var2
var5 = var1 * var2
var6 = var1 / var2




# === Exercise 3. === #

# Convert a_random_number to string
a_random_number = 25
a_random_number = str(a_random_number)

# Convert a_random_string to int
a_random_string = "334"
a_random_string = int(a_random_string)

# Convert a_random_string2 to float
a_random_string2 = "135.25"
a_random_string2 = float(a_random_string2)




# === Exercise 4. === #

my_first_name = "Maiko"
my_last_name = "Metsalu"
my_middle_name = "Polegi"

# Make a variable named "full_name" which should be "Maiko Polegi Metsalu"
# Make a variable called "formal_name" which should be "Metsalu, Polegi, Maiko"
full_name = f"{my_first_name} {my_middle_name} {my_last_name}"
formal_name = f"{my_last_name}, {my_middle_name}, {my_first_name}"

# === Exercise 5. === #

my_age = 21
# Using the last variables, print out "Hi, Maiko Polegi Metsalu, you are 21 years old and your formal name is
# Metsalu, Polegi, Maiko"
print(f"Hi, {full_name}, you are {str(my_age)} years old and your formal name is {formal_name}")

# === Exercise 6. === #

# Make a variable called "birth_year" and calculate what year was I born. Do not change the current_year type.
current_year = "2020"
birth_year = int(current_year) - 21




# === Exercise 7. === #

# Find what is the first letter in a_random_string3 and print it out
# Find what is the 26th letter in a_random_string3 and print it out
# Find what is the last letter in a_random_string3 and print it out
# Print out the first 20 letters of a_random_string3
# Print out the letters from 10 to 15 in a_random_string3
# Find if "Maikoko" is in a_random_string3 and print it out ==> hint: print("a" in a_random_string)
a_random_string3 = "MaikoMaikoMMaikMaikooaiMaMaikoMaikoikoMaikokoMaMaikoMaMaikoikoMaikoiMaikoMaikokoMaMMaikoaiMaikokoi"
print(a_random_string3[0])
print(a_random_string3[25])
print(a_random_string3[-1])



# === Exercise 8. === #

# Make it so a_random_string4's first letter is in Caps Lock
# Make it so a_random_string4 is in all caps lock
# Make it so a_random_string4 is in all small letters
# Find out if a_random_string4 starts with "tava" and print it out ==> hint: print(a_random_string.startswith("a"))
# Find out if a_random_string4 ends with "ttt" and print it out
a_random_string4 = "tavaline tekst"



# === Exercise 9. === #

# Print if a_random_nr is equal to a_random_nr2
# Print if a_random_nr is equal to or less than a_random_nr2

a_random_nr = 25
a_random_nr2 = 25




# === Exercise 10. === #

# If a_new_age is less than 10, print "You are too young to enter!
# If a_new_age is less than or equal to 15, print "You can enter only with your parents!"
# If a_new_age is over 15, print "You can enter alone!"
a_new_age = int(input("Sisesta vanus"))


# === Exercise 11. === #

# If a_new_name has over 15 letters in it, print "Your name is too long"
# If a_new_name has 10 letters in it and a_new_age is 10, print "You are a golden person!"
# If a_new_name has less than 3 letters, print "Your name is weird!"
a_new_name = input("Sisesta nimi:")
