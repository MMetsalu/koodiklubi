import random


def roll_dice(side_amt):
    random_number = random.randint(1, int(side_amt) + 1)
    return str(random_number)


print("See on täringumäng.")
print("Kui enam mängida ei taha, kirjuta 'exit'")
sides = input("Mitu numbrit sa täringule tahad? (Nt 6, 24, 100, 1000)")
print("Kirjuta 'veereta' et veeretada täringut!")
command = ""
while command != "exit":
    command = input()
    if command.lower() == "veereta":
        print("Sa veeretasid " + roll_dice(sides) + ".")
