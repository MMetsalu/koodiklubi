def is_prime_number(number: int) -> bool:
    if number > 1:
        if number == 2:
            return True
        for i in range(2, number):
            if number % i == 0:
                return False
        return True


command = ""
while command != "exit":
    command = input("Sisesta mingi arv, ja ma ütlen, kas see on algarv:")
    if command != "exit":
        if is_prime_number(int(command)):
            print("See arv on tõesti algarv!")
        else:
            print("See arv ei ole algarv!")