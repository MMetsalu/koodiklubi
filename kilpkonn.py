"""Õpime kilpkonna graafikat."""

import turtle

# Teeme uue kilpkonna
kilpkonn = turtle.Turtle()

# Kilpkonn saab liikuda edasi kasutades funktsiooni forward()
# Näiteks kilpkonn.forward(50) - kilpkonn kõnnib 50 pikslit edasi

# Kilpkonn saab keerata paremale ja vasakule
# Selleks on meetodid kilpkonn.left() ja kilpkonn.right()
# NB!! left ja right sulgudesse tuleb panna mitu kraadi keerata tahad!
# Näiteks kilpkonn.left(90) - kilpkonn keerab 90 kraadi vasakule

# Kui sa tahad et kilpkonn liiguks vahepeal nii, et ta ei joonista, tee kilpkonn.up()
# Kui sa tahad et kilpkonn jälle joonistaks, tee kilpkonn.down()

# Ülesanne 1
def joonista_ruut(kulje_pikkus: int):
    """See meetod joonistab kilpkonnaga ekraanile ruudu.
    :param kulje_pikkus - ruudu külje pikkus
    """
    for i in range(0, 5):
        kilpkonn.forward(kulje_pikkus)
        kilpkonn.left(90)


# Ülesanne 2
def joonista_kolmnurk(kulje_pikkus: int):
    """See meetod joonistab kilpkonnaga ekraanile kolmnurga.
        :param kulje_pikkus - kolmnurga külje pikkus
        """
    # TODO - tee nii et kilpkonn joonistaks kolmnurga


# Ülesanne 3
def joonista_maja():
    """
    See meetod joonistab kilpkonnaga ekraanile ruudu ja kolmnurga
    Maja katus ehk kolmnurk võiks olla natuke suurem kui maja
    """
    # TODO - tee nii et kilpkonn joonistaks maja


# Ülesanne 4
def joonista_spiraal():
    """
    See meetod joonistab ekraanile spiraali
    """
    # TODO - tee nii et kilpkonn joonistaks spiraali


# See peab olema viimane rida koodi.
# See rida teeb nii et ekraan kinni ei läheks kohe.

if __name__ == '__main__':
    print()
    kilpkonn.color(" ")
    kilpkonn.begin_fill()
    joonista_ruut(10)
    kilpkonn.end_fill()
    kilpkonn.getscreen().exitonclick()