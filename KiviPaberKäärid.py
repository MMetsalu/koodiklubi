"""Simple version of rock paper and scissors."""
from random import choice

def normalize_user_name(name: str) -> str:
    """
    Simple function gets player name as input and capitalizes it.

    :param name: name of the player
    :return: A name that is capitalized.
    """
    return name.capitalize()
    pass


def reverse_user_name(name: str) -> str:
    """
    Function that takes in name as a parameter and reverses its letters. The name should also be capitalized.

    :param name: name of the player
    :return: A name that is reversed.
    """
    name = name[::-1]
    return normalize_user_name(name)
    pass


def check_user_choice(choice: str) -> str:
    """
    Function that checks user's choice.

    The choice can be uppercase or lowercase string, but the choice must be
    either rock, paper or scissors. If it is, then return a choice that is lowercase.
    Otherwise return 'Sorry, you entered unknown command.'
    :param choice: user choice
    :return: choice or an error message
    """
    choice = choice.lower()
    if choice == "kivi" or choice == "paber" or choice == "käärid":
        return choice
    else:
        return "Vabandust, sa sisestasid vale käsu!"
    pass


def determine_winner(name: str, user_choice: str, computer_choice: str, reverse_name: bool = False) -> str:
    """
    Determine the winner returns a string that has information about who won.

    You should use the functions that you wrote before. You should use check_user_choice function
    to validate the user choice and use normalize_user_name for representing a correct name. If the
    function parameter reverse is true, then you should also reverse the player name.
    NB! Use the previous functions that you have written!

    :param name:player name
    :param user_choice:
    :param computer_choice:
    :param reverse_name:
    :return:
    """
    if reverse_name:
        name = reverse_user_name(name)
    else:
        name = normalize_user_name(name)
    user_choice = user_choice.lower()
    computer_choice = computer_choice.lower()

    if user_choice == computer_choice:
        output = name + " pani " + user_choice + " ja arvuti pani " + computer_choice + ", seega jäite viiki!"
    else:
        if ((user_choice == "paber" and computer_choice == "kivi")
                or (user_choice == "kivi" and computer_choice == "käärid")
                or (user_choice == "käärid" and computer_choice == "paber")):
            output = name + " pani " + user_choice + " ja arvuti pani " + computer_choice + ", järelikult " \
                + name + " võitis."
        elif ((computer_choice == "paber" and user_choice == "kivi")
                or (computer_choice == "kivi" and user_choice == "käärid")
                or (computer_choice == "käärid" and user_choice == "paber")):
            output = name + " pani " + user_choice + " ja arvuti pani " + computer_choice + ", järelikult arvuti" \
                " võitis."
        else:
            output = "Võitjat ei saanud tuvastada."
    return output


def play_game() -> None:
    """
    Enable gameplay of the game you just created.

    :return:
    """
    user_name = input("Mis su nimi on? ")
    play_more = True
    while play_more:
        computer_choice = choice(['kivi', 'paber', 'käärid'])
        user_choice = check_user_choice(input("Mis sa valid (kivi, paber või käärid)? "))
        print(determine_winner(user_name, user_choice, computer_choice))
        play_more = True if input("Kas sa tahad veel mängida ? [jah / ei] ").lower() == 'jah' else False


if __name__ == "__main__":
    play_game()
