import random
import time

print("See on veidi lahedam Täringumäng!")
print("Sa pead täringuga veeretama 21 või vähem.")
print("Kui veeretad rohkem, oled automaatselt kaotanud.")
print("Kirjuta 'veereta' et veeretada ja 'stop' kui rohkem veeretada ei taha")
print("Peale sind veeretab arvuti. Siis näed kes võidab.")


def roll_dice():
    return random.randint(1, 7)


player_total = 0
computer_total = 0
command = ""
while command.lower() != "stop":
    command = input()
    if command.lower() == "veereta":
        veeretus = roll_dice()
        player_total += veeretus
        print("sa veeretasid " + str(veeretus) + " ja oled kokku veeretanud " + str(player_total))
        print("Kirjuta 'veereta' et veel veeretada või 'stop' kui oled oma tulemusega rahul.")
        if player_total > 21:
            print("Sa kaotasid mängu!")
            exit(0)

while computer_total < 16:
    veeretus = roll_dice()
    computer_total += veeretus
    print("Arvuti veeretas " + str(veeretus) + " ja on kokku veeretanud " + str(computer_total))
    time.sleep(2)
    if computer_total > 21:
        print("Arvuti kaotas! Sina võitsid!")
        break
    elif computer_total > player_total:
        print("Arvuti veeretas sinust rohkem ja võitis!")
        break
    elif computer_total == player_total:
        print("Arvuti veeretas sinuga sama palju! Te jäite viiki!")
        break
    else:
        print("Arvuti rohkem veeretada ei tahtnud! Sina võitsid!")
        break


