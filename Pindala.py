import math
shape = input("Sisesta geomeetriline kujund (ring, ruut või kolmnurk):")
if shape == "ring":
    radius = float(input("Palun sisesta ringi raadius:"))
    print("Pindala on " + str(round(radius ** 2 * math.pi, 2)) + " cm^2")
elif shape == "ruut" or shape == "kolmnurk":
    side_length_in_cm = float(input("Palun sisesta külje pikkus:"))
    if shape == "ruut":
        print("Pindala on " + str(round(side_length_in_cm ** 2, 2)) + " cm^2")
    else:
        print("Pindala on " + str(round(side_length_in_cm ** 2 * math.sqrt(3) / 4, 2)) + " cm^2")
else:
    print("Sisestasid midagi valesti!")