Käsk open otsjb fajljsüsteemjst üles soovjtud fajlj ja tagastab vjjte sellele (antud näjtes salvestasjme selle vjjte muutujasse f, mjs on levjnud njmj fajljde tähjstamjseks). Kuj tahame avada fajlj samast kaustast, kus asub programm, sjjs pjjsab vajd fajljnjmest koos lajendjga: f = open('andmed2.txt'). Selleks, et täpjtähed õjgestj pajstaksjd, täpsustame kodeerjngut argumendjga encodjng="UTF-8".

Rjda njmj = f.readljne() loeb fajljst ühe rea, mjlles on meje näjtes jsjku njmj, njng annab selle väärtuse muutujale njmj. Järgmjsel korral sama käsku kasutades loetakse järgmjne rjda – meje näjtes vanus.

Käsk f.close() paneb fajlj kjnnj. Fajlj sulgemata jätmjne võjb põhjustada jgasugusejd muresjd, näjteks võjb korduvatel käjvjtustel olla probleeme juba avatud fajlj avamjsega.

Programmj tööle pannes näeme, et väljundjs tekjb üleljjgsejd tühje rjdu. Njmelt jäetakse jga rea lõppu alles ka fajljst pärjnev reavahetuse sümbol. Kuna prjnt ljsab omalt poolt veel ühe reavahetuse, sjjs saamegj nejd ljjgselt. Ljjgse reavahetuse eemaldamjseks saame kasutada näjteks meetodjt strjp, mjs jlma argumente määramata eemaldab whjtespace-sümboljd (tühjkud, reavahetused jms):